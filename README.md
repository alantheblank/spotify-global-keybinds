# Spotify Global Keybinds

I play games, I like to listen to music while playing games, tabbing out can cause issues.  Play, skip, and change volume with this script..

## Installation
Installation is simple.  First [python 3.10](https://www.python.org/downloads/) needs to be installed, previous versions may work but I developed this on python 3.10.
Clone this repo using [git](https://git-scm.com/download/win) with
`git clone https://gitlab.com/alantheblank/spotify-global-keybinds.git`
next, using [pip](https://pip.pypa.io/en/stable/installation/) install the required packages listed in the requirements.txt by running `python -m pip install -r requirements.txt`

Lastly you need to get api keys from spotify by [making an application](https://developer.spotify.com/dashboard/applications), set the redirect url to something like `http://localhost:8888/callback` (this is already set in the example api file).  Included is an example api json file, simply fill the fields and change the name to api.json.

## Usage
running the script is easy, first set the keybinds in keybinds.json, they are by default F13-F17 as I've set it up for use with the Razer Naga X which can use the extended function keys
to run the script use `python main.py`

## Support
it should work straight off the bat but if it doesn't then feel free to open an issue and I'll try help

## Contributing
I suppose if you want to add to it make a fork or something, this is suitable for my needs however if you wish to improve what is written then feel free to submit a PR
