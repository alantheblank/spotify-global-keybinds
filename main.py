import json

import keyboard
import spotipy
from spotipy.oauth2 import SpotifyOAuth
from spotipy.exceptions import SpotifyException

from keyboard import add_hotkey

class Spotify:

    vol: int = 20
    sp: spotipy.Spotify = None
    device_id: int = None

    def __init__(self):
        self.init_keybinds()
        keys = self.get_api()
        self.sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=keys['client_id'],
                                                            client_secret=keys['client_secret'],
                                                            redirect_uri=keys['redirect_url'],
                                                            scope=keys['scope']))
        self.vol = self.sp.devices()['devices'][0]['volume_percent']

    @staticmethod
    def get_api() -> dict[str, str]:
        with open("api.json", "r") as f:
            data = json.load(f)
            return data

    def init_keybinds(self) -> None:
        binds = self.get_keybinds()
        keyboard.on_press_key(binds['vol_up'], self.increase_vol)
        #add_hotkey(binds['vol_up'], self.increase_vol)
        keyboard.on_press_key(binds['vol_down'], self.decrease_vol)
        keyboard.on_press_key(binds['toggle_play'], self.playback)
        keyboard.on_press_key(binds['next'], self.next_song)
        keyboard.on_press_key(binds['prev'], self.prev_song)

    def next_song(self, event) -> None:
        try:
            self.sp.next_track()
        except SpotifyException:
            return

    def prev_song(self, event) -> None:
        try:
            self.sp.previous_track()
        except SpotifyException:
            return

    @staticmethod
    def get_keybinds() -> dict[str, str]:
        with open("keybinds.json", "r") as f:
            data = json.load(f)
            return data

    def playback(self, event) -> None:
        try:
            self.sp.pause_playback()
        except SpotifyException:
            # As far as I can see, the error message can not be suppressed
            try:
                self.sp.start_playback()
            except SpotifyException:
                return

    def increase_vol(self, event) -> None:
        self.vol += 2
        if self.vol > 100:
            self.vol = 100
        # Is debug text needed here? It might be nice for end user to know percentage
        print(self.vol)
        try:
            self.sp.volume(self.vol)
        except SpotifyException:
            return

    def decrease_vol(self, event) -> None:
        self.vol -= 2
        if self.vol < 0:
            self.vol = 0
        print(self.vol)
        try:
            self.sp.volume(self.vol)
        except SpotifyException:
            return


if __name__ == "__main__":
    Spotify()
    input("Press enter to quit\n")